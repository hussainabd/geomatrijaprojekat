object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 659
  ClientWidth = 980
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Image1: TImage
    Left = 16
    Top = 16
    Width = 737
    Height = 561
  end
  object Label1: TLabel
    Left = 784
    Top = 16
    Width = 96
    Height = 18
    Caption = 'Broj ta'#269'kaka : '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object pointsNumberInput: TEdit
    Left = 784
    Top = 40
    Width = 121
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  object generatePoints: TButton
    Left = 16
    Top = 600
    Width = 177
    Height = 41
    Caption = 'Generisi ta'#269'ke'
    TabOrder = 1
    OnClick = generatePointsClick
  end
  object findCircle: TButton
    Left = 199
    Top = 600
    Width = 177
    Height = 41
    Caption = 'Odredi kru'#382'nicu'
    TabOrder = 2
    OnClick = findCircleClick
  end
  object clear: TButton
    Left = 400
    Top = 600
    Width = 177
    Height = 41
    Caption = 'Clear'
    TabOrder = 3
    OnClick = clearClick
  end
end
