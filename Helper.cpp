//---------------------------------------------------------------------------

#pragma hdrstop

#include "Helper.h"

void MyPoint::draw(TImage *image) const {
	image->Canvas->Ellipse(x-2, y-2, x+2, y+2);
}

void MyCircle::draw(TImage *image) const {
	image->Canvas->Ellipse(center.x - radius, center.y - radius, center.x + radius, center.y + radius);
}

bool operator < (MyPoint firstPoint, MyPoint secondPoint) {
	if(firstPoint.x < secondPoint.x)
	  return true;
	if(firstPoint.x == secondPoint.x)
	  return firstPoint.y < secondPoint.y;
	return false;
}

bool operator == (MyPoint firstPoint, MyPoint secondPoint) {
  return firstPoint.x == secondPoint.x && firstPoint.y == secondPoint.y;

}

//Check whether the point is inside the circle
bool isInside(MyCircle circle, MyPoint point)
{
	// Compare radius of circle with distance
	if ((point.x - circle.center.x) * (point.x - circle.center.x) +
		(point.y - circle.center.y) * (point.y - circle.center.y) <= circle.radius * circle.radius)
		return true;
	else
		return false;
}

//Generate circle using 3 points
MyCircle findCircleAlgorithm(MyPoint firstPoint, MyPoint secondPoint, MyPoint thirdPoint)
{
	int x12 = firstPoint.x - secondPoint.x;
	int x13 = firstPoint.x - thirdPoint.x;

	int y12 = firstPoint.y - secondPoint.y;
	int y13 = firstPoint.y - thirdPoint.y;

	int y31 = thirdPoint.y - firstPoint.y;
	int y21 = secondPoint.y - firstPoint.y;

	int x31 = thirdPoint.x - firstPoint.x;
	int x21 = secondPoint.x - firstPoint.x;

	// x1^2 - x3^2
	int sx13 = pow(firstPoint.x, 2) - pow(thirdPoint.x, 2);

	// y1^2 - y3^2
	int sy13 = pow(firstPoint.y, 2) - pow(thirdPoint.y, 2);

	int sx21 = pow(secondPoint.x, 2) - pow(firstPoint.x, 2);
	int sy21 = pow(secondPoint.y, 2) - pow(firstPoint.y, 2);

	int f = ((sx13) * (x12)
			 + (sy13) * (x12)
			 + (sx21) * (x13)
			 + (sy21) * (x13))
			/ (2 * ((y31) * (x12) - (y21) * (x13)));
	int g = ((sx13) * (y12)
			 + (sy13) * (y12)
			 + (sx21) * (y13)
			 + (sy21) * (y13))
			/ (2 * ((x31) * (y12) - (x21) * (y13)));

	int c = -pow(firstPoint.x, 2) - pow(firstPoint.y, 2) - 2 * g * firstPoint.x - 2 * f * firstPoint.y;

	// eqn of circle be x^2 + y^2 + 2*g*x + 2*f*y + c = 0
	// where centre is (h = -g, k = -f) and radius r
	// as r^2 = h^2 + k^2 - c
	int h = -g;
	int k = -f;
	int sqr_of_r = h * h + k * k - c;

	// r is the radius
	float r = sqrt(sqr_of_r);

	MyPoint center(h, k);
	MyCircle circle(center, r);

	return circle;
}

//---------------------------------------------------------------------------

#pragma package(smart_init)
