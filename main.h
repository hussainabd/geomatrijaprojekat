//---------------------------------------------------------------------------
#ifndef mainH
#define mainH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include "Helper.h"
#include <vector>
#include <set>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:
	TImage *Image1;
	TLabel *Label1;
	TEdit *pointsNumberInput;
	TButton *generatePoints;
	TButton *findCircle;
	TButton *clear;
	void __fastcall generatePointsClick(TObject *Sender);
	void __fastcall findCircleClick(TObject *Sender);
	void __fastcall clearClick(TObject *Sender);

private:
	vector<MyPoint> pointList;
	vector<CircleAndPoints> circleList;
	void addPoint(MyPoint);
	bool isPointsNumIsEqual(MyCircle, MyPoint, MyPoint, MyPoint);

public:
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
