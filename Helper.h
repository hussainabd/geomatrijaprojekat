//---------------------------------------------------------------------------

#ifndef HelperH
#define HelperH
#include <vector>
using namespace std;

struct MyPoint {

	int x;
	int y;

	MyPoint() {
		x = 0;
		y = 0;
	}

	MyPoint(int X, int Y) {
		x = X;
		y = Y;
	}

	void draw(TImage*) const;
};

struct MyCircle {

	MyPoint center;
	float radius;

	MyCircle() {
		MyPoint point(0, 0);
		center = point;
		radius = 0;
	}

	MyCircle(MyPoint pCenter, float pRadius) {
		center = pCenter;
		radius = pRadius;
	}

	void draw(TImage*) const;
};

struct CircleAndPoints {

	MyCircle circle;
	MyPoint firstPoint;
	MyPoint secondPoint;
	MyPoint thirdPoint;

	CircleAndPoints() {
		MyPoint point(0, 0);
		MyCircle myCircle(point, 0);

		circle = myCircle;
		firstPoint = point;
		secondPoint = point;
		thirdPoint = point;
	}

	CircleAndPoints(MyCircle pCircle, MyPoint pFirstPoint, MyPoint pSecondPoint, MyPoint pThirdPoint) {
		circle = pCircle;
		firstPoint = pFirstPoint;
		secondPoint = pSecondPoint;
		thirdPoint = pThirdPoint;
	}
};

bool operator < (MyPoint, MyPoint);
bool operator == (MyPoint, MyPoint);
bool isInside(MyCircle, MyPoint);
MyCircle findCircleAlgorithm(MyPoint, MyPoint, MyPoint);

//---------------------------------------------------------------------------
#endif